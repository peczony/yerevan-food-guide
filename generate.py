#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import os
import re
import shutil
from pathlib import Path

FOLDER = os.path.dirname(os.path.abspath(__file__))
KEYS = ["name", "link", "tags", "recommend", "slug", "date"]
TRANSLIT = json.loads((Path(FOLDER) / "translit.json").read_text())
NO_BREAK_SEQUENCES = [
    "а",
    "без",
    "в",
    "во",
    "где",
    "для",
    "же",
    "за",
    "и",
    "или",
    "из",
    "из-за",
    "к",
    "как",
    "на",
    "над",
    "не",
    "ни",
    "но",
    "о",
    "от",
    "по",
    "под",
    "при",
    "с",
    "со",
    "то",
    "у",
    "что",
    "перед",
]
NO_BREAK_SEQUENCES_LEFT = ["бы", "ли", "же", "—", "–"]
re_nbh = re.compile(
    "(^|[^а-яё])(?P<word>[а-яё]{0,3}\\-[а-яё]{0,3})([^а-яё]|$)", flags=re.I
)


def replace_no_break_spaces(s):
    for sp in NO_BREAK_SEQUENCES + [x.title() for x in NO_BREAK_SEQUENCES]:
        r_from = "(^|[ \u00a0]){sp} ".format(sp=sp)
        r_to = "\\g<1>{sp}\u00a0".format(sp=sp)
        s = re.sub(r_from, r_to, s)
    for sp in NO_BREAK_SEQUENCES_LEFT + [x.title() for x in NO_BREAK_SEQUENCES_LEFT]:
        r_from = " {sp}([ \u00a0]|$)".format(sp=sp)
        r_to = "\u00a0{sp}\\g<1>".format(sp=sp)
        s = re.sub(r_from, r_to, s)
    srch = re_nbh.search(s)
    while srch:
        s = s.replace(
            srch.group("word"), srch.group("word").replace("-", "\u2011")
        )  # non-breaking hyphen
        srch = re_nbh.search(s)
    return s


def parse_single_place(pl):
    result = {}
    lines = pl.split("\n")
    text = []
    for line in lines:
        if not line.strip():
            continue
        sp = line.split(":", 1)
        if sp[0] in KEYS:
            result[sp[0]] = sp[1].strip()
        else:
            text.append(line.strip())
    result["tags"] = sorted([x.strip() for x in result["tags"].split(",") if x.strip()])
    result["text"] = "\n".join(text)
    return result


def parse_places(cnt):
    sp = cnt.split("---")
    result = []
    for pl in sp:
        result.append(parse_single_place(pl.strip()))
    return result


TAGS_STUB = """<div class="buttons"><p>Теги: 
    <a href="#" onclick="show_category('.coffee')()">кофе</a>,
    <a href="#" onclick="show_category('.food')()">еда</a>,
    <a href="#" onclick="show_category('.breakfast')()">завтрак</a>
</p></div>"""


def translit(str_):
    out = "".join([(TRANSLIT.get(letter.lower()) or letter) for letter in str_])
    return re.sub("[^a-z0-9]", "", out)


def generate_tag(tag, en_tag):
    return f"""<a href="#{en_tag}" class="tag" data-tag=".{en_tag}">{tag}</a>"""


def generate_tags(tags):
    result = []
    for tag in tags:
        result.append(generate_tag(tag, tags[tag]))
    result.append("""<a class="tag" href="#">показать всё</a>""")
    return ", ".join(result)


def generate_place(place, tags):
    class_ = ["place"]
    tags_ = []
    for tag in place["tags"]:
        en_tag = tags.get(tag) or translit(tag)
        class_.append(en_tag)
        tags_.append({"en_name": en_tag, "ru_name": tag})
    result = {
        "title": place["name"],
        "date": place["date"],
        "link": place["link"],
        "slug": place["slug"],
        "text": replace_no_break_spaces(place["text"]),
        "class": " ".join(class_),
        "tags": tags_
    }
    if place.get("recommend"):
        result["recommend"] = place["recommend"]
    return result


def generate_places(places, tags):
    return [generate_place(place, tags) for place in places]


def parse_tags(text):
    result = {}
    for line in text.split("\n"):
        sp = line.strip().split(":", 1)
        if len(sp) == 2:
            result[sp[0].strip()] = sp[1].strip()
    return result


def main():
    places = parse_places((Path(FOLDER) / "places.txt").read_text())
    places = sorted(places, key=lambda x: x["name"])
    tags = parse_tags((Path(FOLDER) / "tags.txt").read_text())

    stub = (Path(FOLDER) / "index_stub.html").read_text()
    stub = stub.replace("{{TAGS}}", generate_tags(tags))
    stub = stub.replace("{{CATEGORIES}}", json.dumps(sorted(tags.values()), indent=4, ensure_ascii=False))
    stub = stub.replace("{{PLACES}}", json.dumps(generate_places(places, tags), indent=4, ensure_ascii=False))

    output_folder = os.path.join(FOLDER, "public")
    shutil.rmtree(output_folder, ignore_errors=True)
    os.makedirs(output_folder, exist_ok=True)
    (Path(output_folder) / "index.html").write_text(stub)
    shutil.copy(
        os.path.join(FOLDER, "custom.css"), os.path.join(output_folder, "custom.css")
    )


if __name__ == "__main__":
    main()
